import  format from 'date-fns/format';
// import  addDays  from 'date-fns/addDays';

export default class Placement {
  constructor( id=null, name="", url="", height=0, width=0, startDate=new Date(), endDate=new Date(), price=0.1, status="", asset="") {

    this.id = id;
    this.name = name;
    this.url = url;
    this.height = height;
    this.width = width;
    this.startDate = startDate;
    this.endDate = endDate;
    this.price = price;
    this.status = status;
    this.asset = asset;
    this.imageBlob ="";
  }

  get formatedStartDate(){
    return format(new Date(this.startDate), "d MMM yyyy").toString();
  }

  get formatedEndDate(){
    return format(new Date(this.endDate), "d MMM yyyy").toString();
  }

  get formData(){
    let formData = new FormData();
    formData.append('id', this.id);
    formData.append('name', this.name);
    formData.append('url', this.url);
    formData.append('height', this.height);
    formData.append('width', this.width);
    formData.append('startDate', this.startDate);
    formData.append('asset', this.asset);

    console.log("[placement] formData", formData.entries(), ...formData);

    return formData;
  }

  get formDataImage(){
    let formData = new FormData();
    formData.append('contentUrl', this.width);
    formData.append('file', this.imageBlob);

    console.log("[placement] formDataImage", formData.entries(), ...formData);

    return formData;
  }
}
