export default class Website {
  constructor( name, url, id=null) {
    this.name = name;
    this.url = url;
    this.id = id;
  }
}
