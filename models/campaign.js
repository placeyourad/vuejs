import format from "date-fns/format";

export default class Campaign {
  constructor(
    id=null, name="", state="",
    startDate=new Date(), endDate=new Date(),
    placements=[]
  ) {
    this.id = id;
    this.name = name;
    this.state = state;
    this.startDate = startDate;
    this.endDate = endDate;
    this.placements = placements;
  }

  get formatedStartDate(){
    return format(new Date(this.startDate), "d MMM yyyy").toString();
  }

  get formatedEndDate(){
    return format(new Date(this.endDate), "d MMM yyyy").toString();
  }

  get updateProposal(){
    return {
      id: this.id,
      placements: this.placements
    }
  }
}
