import Vue from "vue";
import App from "./App.vue";
import {router} from "./routes";
import store from "./store";
import axios from "axios";

import 'materialize-css/dist/css/materialize.min.css';
import 'material-design-icons/iconfont/material-icons.css';

import { ApolloClient } from 'apollo-client'
import { createHttpLink } from 'apollo-link-http'
import { InMemoryCache } from 'apollo-cache-inmemory'
import VueApollo from 'vue-apollo'

Vue.use(VueApollo)


// HTTP connection to the API
const httpLink = createHttpLink({
    // You should use an absolute URL here
    uri: 'http://localhost:4000//graphql',
})

// Cache implementation
const cache = new InMemoryCache()

// Create the apollo client
const apolloClient = new ApolloClient({
    link: httpLink,
    cache,
})

const apolloProvider = new VueApollo({
    defaultClient: apolloClient,
})

Vue.config.productionTip = false;
axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
axios.interceptors.response.use(
    response => response,
    error => {
        console.log("error", error);
        if (error.response.status === 422) {
            store.commit("setErrors", error.response.data.errors);
        } else if (error.response.status === 401) {
            store.commit("auth/setUserData", null);
            localStorage.removeItem("authToken");
            router.push({ name: "Login" });
        } else {
            return Promise.reject(error);
        }
    }
);

axios.interceptors.request.use(function(config) {
    // console.log("[main] intercept", store.getters.getToken);
    config.headers.common = {
        Authorization: `${store.getters.getToken}`,
        "Content-Type": "application/json",
        Accept: "application/json"
    };

    return config;
});

new Vue({
    router,
    // inject apolloProvider here like vue-router or vuex
    apolloProvider,
    store,
    render: h => h(App)
}).$mount("#app");