import SearchPage from "../components/SearchPage";
// import Homepage from "../components/views/Homepage";
import Campaign from "../views/Campaign";
// import CampaignDetails from "../views/CampaignDetails";
// import Placement from "../views/Placement";
import About from "../views/About";
// import Notif from "../views/Notification";
import Website from "../views/Website";
import Register from "../views/Register";
import Login from "../views/Login";
import Verify from "../views/Verify";
import FromTest from "../views/Formtest";
import Profile from "../views/Profile";
import Proposals from "../views/Proposale";

export default [
  {
    path: '*',
    redirect: "/"
  },
  {
    path: "/",
    component: Login,
  },
  {
    name: "Login",
    path: '/login',
    component: Login,
    roles: ["anonyme"]
  },
  {
    name: "Register",
    path: '/register',
    component: Register,
    roles: ["anonyme"]
  },
  {
    path: "/fromtest",
    component: FromTest,
    roles: ["any"]
  },

  {
    name: "Create campaign",
    path: "/newcampaign",
    component: SearchPage,
    roles: ["ROLE_ADVERTISER"]
  },
  {
    name: "Websites",
    path: "/websites",
    component: Website,
    roles: ["ROLE_PUBLISHER"]
  },
  {
    name: "Campaigns",
    path: "/campaign",
    component: Campaign,
    roles: ["ROLE_ADVERTISER", "ROLE_PUBLISHER"]
  },
  {
    name: "Proposals",
    path: "/proposals",
    component: Proposals,
    roles: ["ROLE_PUBLISHER"]
  },
  // {
  //   name: "Notification",
  //   path: "/notification",
  //   component: Notif,
  //   roles: ["ROLE_ADVERTISER", "ROLE_PUBLISHER"]
  // },
  // {
  //   path: "/campaign/:id(\\d+)",
  //   component: CampaignDetails,
  //   roles: ["ROLE_ADVERTISER", "ROLE_PUBLISHER"]
  // },
  // {
  //   path: "/placement/:id(\\d+)",
  //   component: Placement,
  //   roles: ["ROLE_ADVERTISER", "ROLE_PUBLISHER"]
  // },
  {
    path: '/verify/:hash',
    props: true,
    component: Verify,
  },
  {
    name: "About",
    path: "/about",
    component: About,
    roles: ["any"]
  },
  {
    name: "Profil",
    path: "/profil",
    component: Profile,
    roles: ["ROLE_PUBLISHER", "ROLE_ADVERTISER"]
  },
];

