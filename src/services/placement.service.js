import axios from 'axios';
import authHeader from './auth-header';

const server = process.env.VUE_APP_API_SERVER;
const API_URL = process.env[`VUE_APP_${server}_URL`];

class PlacementService {

    getPlacement(id) {
        return axios.get(API_URL + 'placements/'+id, {
          headers: {
            Authorization: authHeader(),
          }
        });
    }

    createPlacement(user, placement , website) {
        console.log("createPlacement",placement.price, user, placement, website);
        // delete placement.id;

        // axios({
        //     method: 'post',
        //     url : API_URL + 'media_objects',
        //     data: placement.formDataImage,
        //     headers: {
        //       Authorization: authHeader(),
        //       'content-type': 'multipart/form-data'
        //     }
        //   }
        // ).then((response) => {
        //   console.log("[create placement] create image response", response);
        // });

        return axios.post(API_URL + 'placements',
          {
              ...placement,
              owner: "/users/"+user.id,
              website: "/websites/"+website.id,
          },
          {
            headers: {
              Authorization: authHeader()
            }
          }).then((response) => {
            console.log("create placement response", response);
        });

    }

  updatePlacement(placement) {
    console.log("updateplacement", placement);

    return axios.put(API_URL + 'placements/'+placement.id,
      {
        ...placement,
      },
      {
        headers: {
          Authorization: authHeader(),
        }
      }).then((response) => {
      console.log("updateplacement response", response);
    });
  }

  getPlacements(filters) {

    let queryFilters = filters.join("&");
    return axios.get(API_URL + `placements?${queryFilters}`,
      {
        headers: {
          Authorization: authHeader(),
        }
      });
  }
}

export default new PlacementService();