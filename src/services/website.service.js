import axios from 'axios';
import authHeader from './auth-header';

const server = process.env.VUE_APP_API_SERVER;
const API_URL = process.env[`VUE_APP_${server}_URL`];

class WebsiteService {

  getWebsite(id) {
    return axios.get(API_URL + 'websites/'+id, {
      headers: {
        Authorization: authHeader(),
      }
    });
  }

  getUserWebsites(user) {
    return axios.get(`${API_URL}websites?owner=users%2F${user.id}`, {
      headers: {
        Authorization: authHeader(),
      }
    });
  }

  createWebsite(user, website) {
    console.log("createWebsite", user, website)
    return axios.post(API_URL + 'websites',
      {
        ...website,
        owner: "/users/"+user.id
      },
      {
        headers: {
          Authorization: authHeader(),
        }
      }).then((response) => {
      console.log("response", response);
    });
  }

  updateWebsite(website) {
    console.log("updateWebsite", website);
    return axios.put(API_URL + 'websites/'+website.id,
      {
        ...website,
      },
      {
        headers: {
          Authorization: authHeader(),
        }
      }).then((response) => {
      console.log("response", response);
    });
  }

  deleteWebsite(website) {
    // let ids = websites.map(web=>web.id);
    // console.log("deleteWebsite", ids, websites);
    return axios.delete(API_URL + 'websites/'+website.id,
      {
        headers: {
          Authorization: authHeader(),
        }
      }).then((response) => {
      console.log("response", response);
    });
  }
}

export default new WebsiteService();