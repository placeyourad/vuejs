import axios from 'axios';
import authHeader from './auth-header';

const server = process.env.VUE_APP_API_SERVER;
const API_URL = process.env[`VUE_APP_${server}_URL`];

class CampaignService {

    getCampaign(id) {
        return axios.get(API_URL + 'campaigns/' + id, {
            headers: {
                Authorization: authHeader(),
            }
        });
    }

    createCampaign(user, campaign) {
        console.log("[createCampaign]", user, campaign);

        return axios.post(API_URL + 'campaigns',
          {
              ...campaign,
              advertiser: "/users/"+user.id,
          },
          {
              headers: {
                  Authorization: authHeader()
              }
          })

    }

    updateCampaign(campaign) {
        console.log("updateCampaign", campaign);
        return axios.put(API_URL + 'campaigns/'+campaign.id,
            {
                ...campaign,
            },
            {
                headers: {
                    Authorization: authHeader(),
                }
            }).then((response) => {
            console.log("response", response);
        });
    }

  deleteProposal(proposal) {
    console.log("updateCampaign", proposal);
    return axios.delete(API_URL + 'proposals/'+proposal.id,
      {
        headers: {
          Authorization: authHeader(),
        }
      }).then((response) => {
      console.log("response delete", response);
    });
  }

  getCampaigns(filters) {
        let queryFilters = filters.join("&");
        return axios.get(API_URL + `campaigns?${queryFilters}`,
            {
                headers: {
                    Authorization: authHeader(),
                }
            });
    }

    sendProposal(user, campaign, placements, publishersIds) {
      console.log("[send proposal]", campaign, placements);

      let formatedPlacements = placements.map((placement)=>{
        return "/placements/"+placement.id;
      });

      return axios.post(API_URL + 'proposals',
        {
          campaign: "/campaigns/"+campaign.id,
          advertiser: "/users/"+user.id,
          placements: formatedPlacements,
          publishers: publishersIds
        },
        {
          headers: {
            Authorization: authHeader()
          }
        }).then((response) => {
        console.log("create proposal response", response);
      });

    }

    getProposals(filters) {
      let queryFilters = filters.join("&");
      return axios.get(API_URL + `proposals?${queryFilters}`,        {
          headers: {
            Authorization: authHeader(),
          }
        });
    }

}

export default new CampaignService();