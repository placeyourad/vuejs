import axios from 'axios';
import authHeader from './auth-header';

const server = process.env.VUE_APP_API_SERVER;
const API_URL = process.env[`VUE_APP_${server}_URL`];

class NotificationService {

    getNotification(id) {
        return axios.get(API_URL + 'notification/' + id, {
            headers: {
                Authorization: authHeader(),
            }
        });
    }

    createNotification(notification) {
        console.log("[createnotification]", notification);

        return axios.post(API_URL + 'notifications',
          {
              ...notification,
          },
          {
              headers: {
                  Authorization: authHeader()
              }
          }).then((response) => {
            console.log("create notification response", response);
        });

    }

    updateNotification(notification) {
        console.log("updatenotification", notification);
        return axios.put(API_URL + 'notifications/'+notification.id,
            {
                ...notification,
            },
            {
                headers: {
                    Authorization: authHeader(),
                }
            }).then((response) => {
            console.log("response", response);
        });
    }

    getNotifications(filters) {
        let queryFilters = filters.join("&");
        return axios.get(API_URL + `notifications?${queryFilters}`,
            {
                headers: {
                    Authorization: authHeader(),
                }
            });
    }
}

export default new NotificationService();